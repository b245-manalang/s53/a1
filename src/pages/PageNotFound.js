import { Link } from "react-router-dom";

export default function PageNotFound() {

	return (
		<div>	
		<h1 className = 'm-4'>Page Not Found</h1>
		<p className = 'm-4'>Go back to the <Link to ='/'>homepage</Link>.</p>

		</div>
		)
}