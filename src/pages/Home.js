import {Fragment} from 'react';

import Banner from '../components/Banner.js';
import Highlights from '../components/Highlights.js';

export default function Home(){

	return(
		<Fragment>
		  <Banner/>
		  <Highlights/>
		</Fragment>
		)
}