
import './App.css';

// import {Container} from 'react-bootstrap';
// import {Fragment} from 'react';
//importing AppNavBar function from the AppNavBar.js
import AppNavBar from './components/AppNavBar.js';
/*import Banner from './Banner.js';
import Highlights from './Highlights.js';*/
import Home from './pages/Home.js';
import Courses from './pages/Courses.js';
import Register from './pages/Register.js';
import Login from './pages/Login.js';
import Logout from './pages/Logout.js';
import PageNotFound from './pages/PageNotFound.js';
//import modules from react-router-dom for the routing

import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';

function App() {
 
  return (
    <Router>
      <AppNavBar/>
      <Routes>

          <Route path ='/' element = {<Home/>}/>
          <Route path ='/courses' element = {<Courses/>}/>
          <Route path ='/login' element = {<Login/>}/>
          <Route path ='/register' element = {<Register/>}/>
          <Route path ='/logout' element = {<Logout/>}/>
          <Route path='*' element={<PageNotFound/>}/>


      </Routes>
    </Router>
  );
}

export default App;
