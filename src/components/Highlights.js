
import Card from 'react-bootstrap/Card';
import {Container, Row, Col} from 'react-bootstrap';

export default function Highlights(){
	return(
		<Container className = 'mt-5'>
			<Row>
			{/*First Card*/}
				<Col>
			 		<Card>
			      
				      <Card.Body>
				        <Card.Title>Learn From Home</Card.Title>
				        <Card.Text>
				          Some quick example text to build on the card title and make up the
				          bulk of the card's content.
				        </Card.Text>
				      
				      </Card.Body>	
				    </Card>
				</Col>
			{/*Second Card*/}
				<Col>
			 		<Card>
			      
				      <Card.Body>
				        <Card.Title>Study Now, Pay Later</Card.Title>
				        <Card.Text>
				          Some quick example text to build on the card title and make up the
				          bulk of the card's content.
				        </Card.Text>
				      
				      </Card.Body>	
				    </Card>
				</Col>

			{/*Third Card*/}
				<Col>
			 		<Card>
			      
				      <Card.Body>
				        <Card.Title>Be Part of Our Community</Card.Title>
				        <Card.Text>
				          Some quick example text to build on the card title and make up the
				          bulk of the card's content.
				        </Card.Text>
				      
				      </Card.Body>	
				    </Card>
				</Col>
			</Row>
		</Container>
 		
	    
	  );
	}