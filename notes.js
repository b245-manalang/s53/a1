// to create a react app,
	// npx create-react-app project-name

// to run our react application
	// npm start

	// files to be removed:
	// from folder src:
		// App.test.js
		// index.css
		// reportWebVitals.js
		// logo.svg

	// we need to delete all the importations of the said files


// the 'import' statement allows us to use the code or export modules from other files similar to how we use the 'require' function in NodeJS


// React JS it applies the concepts of Rendering and mounting in order to display and create components
// Rendering refers to the process of calling/invoking a component returning set of instructions creating DOM

// Mounting is when REACT JS 'renders or displays' the component the initial DOM based on the instructions

// using the event.target.value will capture the value input by the user on our input area;

	//in the dependencies in useEffect
		//1. Single dependency [dependency]
			//on the initial load of the component the side effect/function will triggered and whenever changes occur on ur dependency
		//2. Empty dependency
			//the side effect will only run during the initial load
		//3. Multiple dependency [dependency1, dependency2]
			//the side effect will run during the initial load and whenever the state of the dependencies change


//[Section] react-router-dom
	//for us to be able to use the module/library across all of our pages we have to contain them with Browser/Router
	//Routes - we contain all of the routes in our react-app
	//Route - specific route wherein we will declare the url and also the component or page to